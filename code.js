//Altura e largura do SVG, "container" que irá abrigar os elementos do grafo
var width = 300,
    height = 300;

//Cria o SVG dentro do elemento body na página html
var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

/*
O force é um layout que simula uma "gravidade" entre os elementos que estão relacionados,
forçando a movimentação entre eles de acordo com a interação do usuário.

.charge: (?) peso dos nós em relação ao seu centro de gravidade; o que interfere na velocidade da animação quando interage com o usuário
.linkDistance: distância entre os nós conectados
.size: altura e largura do container
*/
var force = d3.layout.force()
    .charge(-300)
    .linkDistance(100)
    .size([width, height]);

//escala de cores definida pelo D3 (cores dos nós)
var color = d3.scale.category20();
/*
//escala de cores personalizada
var color = d3.scale.ordinal()
  .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00", "#CC9900","#E6CC80","#DB704D", "#33CC33", "#009933", "#FFFFA6", "#19A3D1", "#0099CC", "#CC99FF", "#98abc5, #98abc5", "#668566", "#999966", "#E6B85C"]);
*/

/*
A função d3.json recebe um arquivo JSON e carrega um objeto graph (grafo).
*/
d3.json("graph.json", function(error, graph) {

  /*
  Após carregar o grafo, passa para a variável force os respectivos nós (nodes) 
  e arestas (links). Ao passar essas informações, os elementos de nós e arestas são criados no svg.
  Com a função start() inicia a simulação do layout (animação).
  */

  force
      .nodes(graph.nodes)
      .links(graph.links)
      .start();

  /*
  A variável link seleciona todos as arestas (a partir da classe "link") e acrescenta
  os dados de arestas do grafo e o componente circle; nele insere a classe (atributo) link e 
  define o estilo (disponível no css padrão do D3) com tamanho de acordo com a raiz quadrada. 0:)
  */

  var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", function(d) { return Math.sqrt(d.value);});

  /*
  A variável node tem funcionamento análogo com as propriedades da variável link.
  Exceto pelo atributo r, que é o raio do nó. A função style dessa variável preenche o nó com uma
  cor, de acordo com o número do grupo (d.group). A função call permite que o usuário possa
  arrastar e soltar esse componente. 
  Ainda na variável node, inclue o atributo title o nome do nó (campo name no arquivo JSON). 

  */

  var node = svg.selectAll(".node")
      .data(graph.nodes)
      .enter().append("circle")
      .attr("class", "node")
      .attr("r", 15)
      .style("fill", function(d) { return color(d.group); })
      .call(force.drag);

  node.append("title")
      .text(function(d) { return d.name; });

  /*
  Após configurar os nós e as arestas, utilizamos a função on, um listener que executa funções de acordo
  com os eventos. Esta função recebe como parâmetro o tipo de evento e a função que 
  irá definir o que será feito nos próximos passos. 
  O evento "tick" é responsável pela atualização da posição dos elementos na tela. Caso não seja configurado,
  irá ficar na posição 0x0.
  */

  force.on("tick", function() {
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
  });

  /*
  É possível observar a mudança dos valores e os componentes utilizando o inspecionamento de elementos
  de qualquer navegador.
  No Chrome: clique com o botão direito em cima do componente desejado; em seguida clique em Inspecionar
  Elemento. Na aba Elements é possível acompanhar os valores dos atributos e também a mudança deles
  de acordo com as interações na tela.

  Ressalva sobre o arquivo de dados: 
  Alguns browsers bloqueam o acesso direto do JavaScript a arquivos locais.
  Neste caso, pode ser necessário rodar um servidor web e retornar o conteúdo do arquivo em uma página (tal como webservices).
*/

});
