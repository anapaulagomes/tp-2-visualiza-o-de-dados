### Trabalho Prático 2 da disciplina de Visualização de Dados - DCC-UFMG
Equipe: Ana Paula Gomes, Hugo Santos, Daniel Balbino, Keiller Nogueira

# Force-Directed Graph com D3js

O tutorial a seguir mostra, passo à passo, como criar um [Force-Directed Graph usando D3js](https://github.com/mbostock/d3/wiki/Force-Layout). O tutorial apresenta um versão simples, porém bem detalhada, seguida de outra versão mais completa, que detalha diferentes opções que podem ser usadas no momento de criação deste tipo de visualização. A referência deste layout pode ser encontrado na [documentação da API](https://github.com/mbostock/d3/wiki/API-Reference).

## Parte 1: Primeiros Passos

Nessa primeira etapa apresentaremos uma versão simples com os elementos mínimos necessários para gerar um Force-Directed Graph. Uma versão editável pode ser visto [aqui](http://plnkr.co/edit/oeSiK9TTyCjNNIQiMHbz?p=preview).

Há duas etapas na criação deste tipo de visualização: (i) declaração da estrutura force (própria do D3js) que armazenará o grafo, (ii) associação dos dados presentes nessa estrutura recém declarada com os elementos SVG e HTML através de funções do framework D3js.

Antes de partir para a primeira etapa, devemos reservar o espaço na tela (largura e altura) onde a visualização será criada. 
O código abaixo realiza essa função, definindo as dimensões do SVG, "container" que irá abrigar os elementos do grafo.

~~~javascript
var width = 300,
    height = 300;
~~~


### Passo 1: Estrutura Grafo

O force é um layout que simula uma "gravidade" entre os elementos que estão relacionados, forçando a movimentação entre eles de acordo com a interação do usuário. No trecho de código a seguir, declaramos a estrutura force com alguns atributos iniciais que serão discutidos com mais detalhes na próxima seção.

~~~javascript
var force = d3.layout.force()
    .charge(-300)
    .linkDistance(100)
    .size([width, height]);
~~~

Em seguida, criamos uma estrutura de grafo simples que armazenará uma array de vértices (nodes) e arestas (links). O número de elementos no array de vértices representam a quantidade de nós do grafo. Tais elementos não precisam ser inicializados, como é neste caso. Já o array de arestas armazena os atributos *source* e *target*, que indicam, respectivamente, o índice do vértive de origem e de destino de uma aresta. Mais detalhes sobre a composição dessas estruturas será dada na segunda parte deste tutorial.

~~~javascript
var graph = {};
graph["nodes"] = [{},{},{}];
graph["links"] = [{"source": 0, "target": 1},
                  {"source": 1, "target": 2},
                  {"source": 2, "target": 0}];
~~~

Após declarado o grafo, devemos passar para a variável force os respectivos vértices (*nodes*) e arestas (*links*). Com estes atributos setados, a visualização pode ser iniciada com a função *start()*.

~~~javascript
force
      .nodes(graph.nodes)
      .links(graph.links)
      .start();
~~~

### Passo 2: Associando ao HTML

Uma vez criado a estrutura com que queremos trabalhar, vamos associá-lo ao HTML através de funções do D3js.
O código abaixo cria o SVG dentro do elemento body na página HTML. Importante ressaltar que o SVG precisa de ter seus atributos largura e altura setados, como foi mostrado anteriormente.

~~~javascript
var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);
~~~

A variável *link* seleciona todas as arestas (a partir da classe *link*) no recém declarado SVG e acrescenta os dados de arestas que estão dentro da estrutura grafo (criada anteriormente). A função *enter()* cria no SVG um número de elementos que corresponde ao número total de arestas, nesse caso, três. Esses elementos podem ter diversos atributos, dos quais alguns serão detalhados mais a frente. Porém, neste primeiro momento, os atributos de cada um desses elementos são somente dois: (i) a forma do elemento, que nesse caso corresponde a uma linha ("line"), ou seja, uma [forma básica](https://www.dashingd3js.com/svg-basic-shapes-and-d3js) do D3js, e (ii) sua classe, que corresponde a alguma configuração de estilo, geralmente configurada via CSS. 

~~~javascript
var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("line")
      .attr("class", "link");
~~~

A variável *node* (vértice) tem funcionamento análogo a da variável *link*. Neste caso, cada vértice é criado em formato de círculo, baseado também nas [formas básicas](https://www.dashingd3js.com/svg-basic-shapes-and-d3js) do D3js. Esse círculo exige um atributo a mais, no caso *r*, que corresponde ao raio do círculo (ou do vértice). Além disso, possui ainda o atributo *call*, que permite ao usuário chamar a função *drag*, que cria a habilidade de arrastar e soltar esse componente.

~~~javascript
var node = svg.selectAll(".node")
      .data(graph.nodes)
      .enter().append("circle")
      .attr("class", "node")
      .attr("r", 10)
      .call(force.drag);
~~~

Após configurar os nós e as arestas, utilizamos a função *on*, um listener que executa funções de acordo com algum evento que ocorra no SVG.
Esta função recebe dois parâmetros: (i) o tipo de evento, e (ii) uma outra função que irá definir o que será feito quando o evento ocorre.
Neste caso, o evento é o *tick*, que captura informações da tela a todo momento, e a função definida é responsável pela atualização da posição dos elementos, tanto da aresta quanto do vértice, na tela. Caso não seja configurado, irá ficar na posição 0x0.

~~~javascript
  force.on("tick", function() {

      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
  });
~~~

**Dica:** 
Caso esteja usando o Chrome, clique com o botão direito em cima do componente desejado; em seguida clique em Inspecionar Elemento. Na aba Elements é possível acompanhar os valores dos atributos e também a mudança deles de acordo com as interações na tela. Nos outros navegadores o processo é parecido.

É possível observar a mudança dos valores e os componentes inspencionando os elementos usando qualquer navegador.
Abaixo, segue HTML gerado por esse tutorial extraído diretamente do navegador. Repare nos elementos *circle* e *line*, que correspondem ao vértice e a aresta do grafo, respectivamente. Há também os atributos de posição, adicionados pela função *force.on* para todos os elementos.

~~~HTML
<html style=""><head><meta charset="utf-8">

<title>Iniciando com Force-Directed Graphs</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<script src="http://d3js.org/d3.v3.min.js"></script>
	<script src="code.js" type="text/javascript"></script>
	
	<svg height="300" width="300">
		<circle cy="205.2272406209413" cx="164.67291241150525" r="10" class="node"></circle>
		<circle cy="135.08585162453522" cx="94.92526272449079" r="10" class="node"></circle>
		<circle cy="109.75615234964808" cx="190.54254106150825" r="10" class="node"></circle>
		<line y2="135.08585162453522" x2="94.92526272449079" y1="205.2272406209413" x1="164.67291241150525" style="stroke-width: 3;" class="link"></line>
		<line y2="109.75615234964808" x2="190.54254106150825" y1="135.08585162453522" x1="94.92526272449079" style="stroke-width: 3;" class="link"></line>
		<line y2="205.2272406209413" x2="164.67291241150525" y1="109.75615234964808" x1="190.54254106150825" style="stroke-width: 3;" class="link"></line>	
	</svg>

</body>
</html>
~~~

## Parte 2: Aprofundando

Nesta segunda parte um novo exemplo mais completo será detalhado. Algumas partes similares à primeira parte deste tutorial serão omitidas sendo explicado somente as diferenças que são significativas. 
Um exemplo editável desta parte do tutorial pode ser encontrado [aqui](http://plnkr.co/edit/RIGGxDHNcTJrrQuVtAZN?p=preview).

### Importando de um arquivo JSON

Na primeira parte deste tutorial criamos um grafo bem simples declarando cada vértice e aresta. Porém, no caso de um grafo grande, a melhor opção é criar um arquivo JSON e importá-lo para dentro do seu programa. Isso pode ser feito facilmente usando a função abaixo, onde o primeiro argumento é o nome do arquivo JSON e o segundo é uma função que irá carregar os dados em suas variáveis (como anteriormente) a partir do seu próprio argumento graph.

~~~javascript
d3.json("graphSimple.json", function(error, graph) {
~~~

#### Variação no layout

A declaração completa de um layout **force** com seus valores:

~~~javascript
var force = d3.layout.force()
    .nodes(graph.nodes)
    .links(graoh.links)
    .size([width, height])
    .linkStrength(0.1)
    .friction(0.9)
    .linkDistance(20)
    .charge(-30)
    .gravity(0.1)
    .theta(0.8)
    .alpha(0.1)
    .start();
~~~

Define-se os seguintes elementos:

* **nodes** : A estrutura *nodes* deve conter uma array de objetos onde cada um representará um nó no grafo. O índice do nó é indicado pelo campo *index* do objeto e, caso não seja indicado, será automaticamente atribuído a partir do índice no vetor;
* **links** : A estrutura *links* deve conter um array de objetos onde cada um representa uma aresta no grafo. Cada objeto deve conter os campos *source* e *target* que indica de qual vértice a aresta sai e para qual ela incide, respectivamente;
* **linkStrength** : Define a intensidade da força de atração/repulsão para manter a distância entre os nós definidas em *linkDistance*;
* **friction** : Define a força de atrito que causará a perda de velocidade dos nós;
* **linkDistance** : Distância entre os dois nós que será reforçada pelas arestas. Quando a distância entre um par de nós for maior que *linkDistance* a força será de atração; caso contrário, será de repulsão;
* **charge** : Quando o valor é negativo, define a força de repulsão entre os objetos;
* **gravity** : Define a força de atração entre os objetos, quanto maior o valor maior a força;
* **start** : Inicia a visualização dos dados.

#### Variação no estilo do SVG

Para definir uma escala de cores para cada vértice, podemos usar uma [função](http://bl.ocks.org/aaizemberg/78bd3dade9593896a59d) definida pelo próprio D3js, como mostrado no código abaixo. Neste caso, temos 20 possíveis cores diferentes para o vértices.

~~~javascript
  var color = d3.scale.category20();
~~~

A criação de arestas é feita de modo bem similar ao usado na parte 1 deste tutorial, com uma única diferença: a definição de um estilo (*style*) para a linha que, nesse caso, estabelece a largura de cada linha tem um valor específico determinado pelo arquivo JSON. O segundo argumento do atributo *style* é uma função que itera sobre todos os dados carregados do JSON que recupera o *value* e configura como largura do elemento correspondente.

~~~javascript
  var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", function(d) { return (d.value);});
~~~

Para criar os vértices, usamos a mesma ideia aplicada na parte 1 deste tutorial porém com uma única diferença na função *style*, que preenche o vértice com uma cor de acordo com o número do grupo (d.group), vinda do arquivo JSON.

~~~javascript
  var node = svg.selectAll(".node")
      .data(graph.nodes)
      .enter().append("circle")
      .attr("class", "node")
      .attr("r", 6)
      .style("fill", function(d) { return color(d.group); })
      .call(force.drag);
~~~

## Extras

Apresentamos aqui, algumas funções extras que podem ser úteis na criação deste tipo de visualização.

### Customizando elementos

Para determinar o atributo dos elementos podemos fazer uso de qualquer [função matemática disponível no javascript](http://www.w3schools.com/js/js_math.asp). Para isso, basta fazer a chamada a função usando a diretiva *Math* passando os parâmetros necessários. No caso do código abaixo, estamos definindo a largura de uma aresta usando a raiz quadrada de seu valor.

~~~javascript
var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", function(d) { return Math.sqrt(d.value);});
~~~

Há ainda a possibilidade de atribuir um nome para cada elemento do SVG. No código abaixo damos a um vértice um atributo *title* que representa seu o nome.

~~~javascript
  node.append("title")
      .text(function(d) { return d.name; });
~~~

### Inserindo rótulos e imagens

Podemos usar [imagens](http://bl.ocks.org/mbostock/950642) e definir [rótulos](http://bl.ocks.org/mbostock/950642) para arestas e vértices de um grafo. No caso de imagens, basta definir o atributo *image* para cada elemento passando a imagem que será usada, junto com sua posição, largura e altura. Um exemplo de uso segue abaixo.

~~~javascript
 node.append("image")
      .attr("xlink:href", "https://github.com/favicon.ico")
      .attr("x", -8)
      .attr("y", -8)
      .attr("width", 16)
      .attr("height", 16);
~~~

No caso de rótulo, basta definir o atributo *text*, passando sua posição.

~~~javascript
node.append("text")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d) { return d.name });
~~~

Podemos usar [símbolos](http://bl.ocks.org/mbostock/1062383) para cada vértices. Para isso basta usar uma função do D3js que já tem símbolos pré-definidos, como no exemplo abaixo.

~~~javascript
 nodes.push({
    type: d3.svg.symbolTypes[~~(Math.random() * d3.svg.symbolTypes.length)],
    size: Math.random() * 300 + 100
  });
~~~