var width = 300,
    height = 300;

var force = d3.layout.force()
    .charge(-300)
    .linkDistance(100)
    .size([width, height]);

var graph = {};
graph["nodes"] = [{},{},{}];
graph["links"] = [{"source": 0, "target": 1},
                  {"source": 1, "target": 2},
                  {"source": 2, "target": 0}];

force
      .nodes(graph.nodes)
      .links(graph.links)
      .start();

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

var node = svg.selectAll(".node")
      .data(graph.nodes)
      .enter().append("circle")
      .attr("class", "node")
      .attr("r", 10)
      .call(force.drag);

var link = svg.selectAll(".link")
      .data(graph.links)
      .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", 3);

  force.on("tick", function() {
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
  });


